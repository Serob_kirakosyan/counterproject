public class Main {
    static int balanc = 10;
    static volatile boolean flag = true;
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (flag) {
                        balanc -= 10;
                        System.out.println(balanc);
                        flag = false;
                    }
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (!flag) {
                        balanc += 10;
                        System.out.println(balanc);
                        flag = true;
                    }
                }
            }
        });
        t1.start();
        t2.start();
    }
}
